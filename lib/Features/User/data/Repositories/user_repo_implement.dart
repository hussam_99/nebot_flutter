// ignore_for_file: depend_on_referenced_packages

import 'package:metal_gate_test/Features/User/data/DataSources/user_local_data_source.dart';
import 'package:metal_gate_test/Features/User/data/DataSources/user_remote_date_source.dart';
import 'package:metal_gate_test/Features/User/data/Models/user_model.dart';
import 'package:metal_gate_test/Features/User/domain/Entities/user.dart';
import 'package:metal_gate_test/Features/User/domain/Repositories/user_repo.dart';
import 'package:metal_gate_test/core/error/exception.dart';
import 'package:metal_gate_test/core/error/failure.dart';
import 'package:metal_gate_test/core/network/network_info.dart';
import 'package:dartz/dartz.dart';

class UserRepoImplement implements UserRepo {
  final UserRemoteDataSource userRemoteDataSource;
  final UserLocalDataSource userLocalDataSource;
  final NetworkInfo networkInfo;
  UserRepoImplement({
    required this.userRemoteDataSource,
    required this.userLocalDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<User>>> getAllUser() async {
    if (await networkInfo.isConnected) {
      try {
        final remotePosts = await userRemoteDataSource.getAllUser();
        return Right(remotePosts);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        return const Right([]);
      } on EmptyCacheException {
        return Left(EmptyCacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, Unit>> logIn(User existUser) async {
    final userModel = UserModel(
      email: existUser.email,
      password: existUser.password,
    );
    if (await networkInfo.isConnected) {
      try {
        await userRemoteDataSource.logIn(userModel);
        return const Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> logOut(User existUser) async {
    final userModel = UserModel(
      email: existUser.email,
      password: existUser.password,
    );
    if (await networkInfo.isConnected) {
      try {
        await userRemoteDataSource.logOut(userModel);
        return const Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> signUp(User newUser) async {
    final userModel = UserModel(
      email: newUser.email,
      password: newUser.password,
      image: newUser.image,
      mobile: newUser.mobile,
      userName: newUser.userName,
    );
    if (await networkInfo.isConnected) {
      try {
        await userRemoteDataSource.logIn(userModel);
        return const Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
