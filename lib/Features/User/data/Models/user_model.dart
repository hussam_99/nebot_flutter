import 'package:metal_gate_test/Features/User/domain/Entities/user.dart';

class UserModel extends User {
  const UserModel({
    super.userId,
    super.password,
    super.userName,
    super.email,
    super.guide,
    super.mobile,
    super.image,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      userId: json['id'],
      userName: json['user_name'],
      email: json['email'],
      guide: json['trip_guide'],
      mobile: json['mobile_phone'],
      image: json['image'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': userId,
      'name': userName,
      'email': email,
      'guide_trip': guide,
      'mobile': mobile,
      'image': image,
    };
  }
}
