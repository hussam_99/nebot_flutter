// ignore_for_file: depend_on_referenced_packages, constant_identifier_names

import 'dart:convert';

import 'package:metal_gate_test/Features/User/data/DataSources/user_local_data_source.dart';
import 'package:metal_gate_test/Features/User/data/Models/user_model.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;
import 'package:metal_gate_test/core/error/exception.dart';

abstract class UserRemoteDataSource {
  Future<List<UserModel>> getAllUser();
  Future<Unit> logOut(UserModel user);
  Future<Unit> signUp(UserModel user);
  Future<Unit> logIn(UserModel user);
}

const BASE_URL = 'http://127.0.0.1:8000/api/';

class UserRemoteDataSourceImplement implements UserRemoteDataSource {
  final http.Client client;
  final UserLocalDataSource userLocalDataSource;

  UserRemoteDataSourceImplement({
    required this.userLocalDataSource,
    required this.client,
  });

  @override
  Future<List<UserModel>> getAllUser() async {
    final String token = await userLocalDataSource.getUserToken();
    final response = await client.get(
      Uri.parse('$BASE_URL/allUsers/'),
      headers: {
        'Content-Type': 'application/',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      final List decodedJson = json.decode(response.body) as List;
      final List<UserModel> postModels = decodedJson
          .map<UserModel>((jsonPostModel) => UserModel.fromJson(jsonPostModel))
          .toList();
      return postModels;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> logIn(UserModel user) async {
    final response = await client.post(Uri.parse('$BASE_URL/login'), headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/',
    }, body: {
      'email': user.email,
      'password': user.password,
    });
    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> logOut(UserModel user) async {
    final String token = await userLocalDataSource.getUserToken();

    final response = await client.post(
      Uri.parse('$BASE_URL/user/logout'),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> signUp(UserModel user) async {
    var headers = {'Accept': 'application/json'};
    var request = http.MultipartRequest(
        'POST',
        Uri.parse(
          '$BASE_URL/api/signup',
        ));
    request.fields.addAll({
      'user_name': user.userName!,
      'email': user.email!,
      'password': user.password!,
      'mobile_phone': user.mobile!,
      'trip_guide': user.guide.toString(),
    });
    if (user.image != null) {
      request.files.add(
        await http.MultipartFile.fromPath(
          'image',
          user.image!,
        ),
      );
    }
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }
}
