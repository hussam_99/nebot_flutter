// ignore_for_file: constant_identifier_names, depend_on_referenced_packages

import 'package:dartz/dartz.dart';
import 'package:metal_gate_test/core/error/exception.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserLocalDataSource {
  Future<String> getUserToken();
  Future<Unit> cachToken(String token);
}

const CACHED_TOKEN = 'CACHED_TOKEN';

class PostLocalDataSourceImplement implements UserLocalDataSource {
  final SharedPreferences sharedPrefrences;
  PostLocalDataSourceImplement(this.sharedPrefrences);

  @override
  Future<Unit> cachToken(String token) {
    sharedPrefrences.setString(
      CACHED_TOKEN,
      token,
    );
    return Future.value(unit);
  }

  @override
  Future<String> getUserToken() {
    final jsonString = sharedPrefrences.getString(CACHED_TOKEN);
    if (jsonString != null) {
      return Future.value(jsonString);
    } else {
      throw EmptyCacheException();
    }
  }
}
