// ignore_for_file: depend_on_referenced_packages

import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int? userId;
  final String? userName;
  final String? password;
  final String? email;
  final bool? guide;
  final String? mobile;
  final String? image;

  const User({
    this.userName,
    this.password,
    this.userId,
     this.email,
     this.guide,
     this.mobile,
    this.image,
  });

  @override
  List<Object?> get props => [userId, email, guide, mobile, password, image];
}
