// ignore_for_file: depend_on_referenced_packages

import 'package:metal_gate_test/Features/User/domain/Entities/user.dart';
import 'package:metal_gate_test/Features/User/domain/Repositories/user_repo.dart';

import '../../../../core/error/failure.dart';

import 'package:dartz/dartz.dart';

class LogInUseCase {
  final UserRepo userRepo;
  LogInUseCase(this.userRepo);
  Future<Either<Failure, Unit>> call(User user) async {
    return userRepo.logIn(user);
  }
}
