// ignore_for_file: depend_on_referenced_packages

import 'package:metal_gate_test/Features/User/domain/Entities/user.dart';
import 'package:metal_gate_test/Features/User/domain/Repositories/user_repo.dart';
import 'package:metal_gate_test/core/error/failure.dart';
import 'package:dartz/dartz.dart';

class GetAllUsersUseCase {
  final UserRepo userRepo;
  GetAllUsersUseCase(this.userRepo);
  Future<Either<Failure, List<User>>> call() async {
    return userRepo.getAllUser();
  }
}
