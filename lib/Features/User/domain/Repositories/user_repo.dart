// ignore_for_file: depend_on_referenced_packages

import 'package:metal_gate_test/Features/User/domain/Entities/user.dart';
import 'package:metal_gate_test/core/error/failure.dart';
import 'dart:async';
import 'package:dartz/dartz.dart';

abstract class UserRepo {
  Future<Either<Failure, Unit>> signUp(User newUser);
  Future<Either<Failure, Unit>> logIn(User existUser);
  Future<Either<Failure, Unit>> logOut(User existUser);
  Future<Either<Failure, List<User>>> getAllUser();
}
