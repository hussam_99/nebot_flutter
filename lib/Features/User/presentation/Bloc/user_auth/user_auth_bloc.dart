// ignore_for_file: type_literal_in_constant_pattern, depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:metal_gate_test/Features/User/domain/Entities/user.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/get_all_users_use_case.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/log_in_use_case.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/log_out_use_case.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/sign_up_use_case.dart';
import 'package:metal_gate_test/core/error/failure.dart';
import 'package:metal_gate_test/core/strings/failures.dart';

part 'user_auth_event.dart';
part 'user_auth_state.dart';

class UserAuthBloc extends Bloc<UserAuthEvent, UserAuthState> {
  GetAllUsersUseCase getAllUsers;
  SignUpUseCase signUp;
  LogInUseCase logIn;
  LogOutUseCase logOut;

  UserAuthBloc(
    this.getAllUsers,
    this.signUp,
    this.logIn,
    this.logOut,
  ) : super(UserAuthInitial()) {
    on<UserAuthEvent>((event, emit) async {
      if (event is GetAllUsersEvent) {
        emit(LoadingUserState());
        final failureOrUsers = await getAllUsers.call();
        failureOrUsers.fold(
          (failure) => ErrorUserState(message: _mapFailureToMessage(failure)),
          (users) => LoadedUsersState(
            users: users,
          ),
        );
      } else if (event is SignUpEvent) {
        emit(LoadingUserState());
        final response = await signUp(event.user);
        emit(
          response.fold(
              (failure) => ErrorUserState(
                    message: _mapFailureToMessage(
                      failure,
                    ),
                  ),
              (_) => const SuccessUserState(message: "")),
        );
      } else if (event is LogInEvent) {
        emit(LoadingUserState());
        final response = await logIn(event.user);
        emit(
          response.fold(
              (failure) => ErrorUserState(
                    message: _mapFailureToMessage(
                      failure,
                    ),
                  ),
              (_) => const SuccessUserState(message: "")),
        );
      } else if (event is LogOutEvent) {
        emit(LoadingUserState());
        final response = await logOut(event.user);
        emit(
          response.fold(
              (failure) => ErrorUserState(
                    message: _mapFailureToMessage(
                      failure,
                    ),
                  ),
              (_) => const SuccessUserState(message: "")),
        );
      }
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case EmptyCacheFailure:
        return EMPTY_CACHE_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}
