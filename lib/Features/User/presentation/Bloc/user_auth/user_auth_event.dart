part of 'user_auth_bloc.dart';

sealed class UserAuthEvent extends Equatable {
  const UserAuthEvent();

  @override
  List<Object> get props => [];
}

class SignUpEvent extends UserAuthEvent {
  final User user;

  const SignUpEvent({required this.user});

  @override
  List<Object> get props => [user];
}

class GetAllUsersEvent extends UserAuthEvent {}

class LogInEvent extends UserAuthEvent {
  final User user;

  const LogInEvent({required this.user});
  @override
  List<Object> get props => [user];
}

class LogOutEvent extends UserAuthEvent {
  final User user;
  const LogOutEvent({required this.user});
  @override
  List<Object> get props => [user];
}
