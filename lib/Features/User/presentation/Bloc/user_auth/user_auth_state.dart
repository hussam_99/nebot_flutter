part of 'user_auth_bloc.dart';

sealed class UserAuthState extends Equatable {
  const UserAuthState();

  @override
  List<Object> get props => [];
}

final class UserAuthInitial extends UserAuthState {}

class LoadingUserState extends UserAuthState {}

class LoadedUsersState extends UserAuthState {
  final List<User>? users;
  const LoadedUsersState({
    required this.users,
  });

  @override
  List<Object> get props => [users ?? []];
}

class ErrorUserState extends UserAuthState {
  final String message;

  const ErrorUserState({required this.message});

  @override
  List<Object> get props => [message];
}

class SuccessUserState extends UserAuthState {
  final String message;

  const SuccessUserState({required this.message});

  @override
  List<Object> get props => [message];
}
