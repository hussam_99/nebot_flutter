// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:metal_gate_test/Features/User/presentation/Widgets/splash/button_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:metal_gate_test/Features/User/presentation/Widgets/splash/social_media_button_widget.dart';
import 'package:metal_gate_test/core/colors/app_colors.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(color: AppColors.blueColor),
        child: Padding(
          padding: EdgeInsets.only(
            top: 100.h,
            bottom: 20.h,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Hi Nebot",
                style: TextStyle(
                  color: AppColors.whiteColor,
                  fontSize: 25.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 15.h,
              ),
              SizedBox(
                width: 250.w,
                child: Text(
                  "Hi NebotHi NebotHi NebotHi NebotHi NebotHi Nebot",
                  style: TextStyle(
                    color: AppColors.whiteColor,
                    fontSize: 16.sp,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 15.h,
              ),
              SplashButton(
                backgroundColor: AppColors.organeColor,
                onpressed: () {
                  Navigator.of(context).pushNamed("/signUp");
                },
                text: "SIGN UP",
              ),
              SizedBox(
                height: 15.h,
              ),
              SplashButton(
                backgroundColor: AppColors.transpatenColor,
                onpressed: () {},
                text: "LOG IN",
                borderColor: AppColors.whiteColor,
              ),
              SizedBox(
                height: 40.h,
              ),
              Text(
                "Or Sign in with",
                style: TextStyle(
                  color: AppColors.whiteColor,
                  fontSize: 10.sp,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SocialMediaButtonWidget(
                    socialIcon: Icons.facebook,
                    onPressed: () {},
                  ),
                  SocialMediaButtonWidget(
                    socialIcon: Icons.email,
                    onPressed: () {},
                  ),
                  SocialMediaButtonWidget(
                    socialIcon: Icons.apple,
                    onPressed: () {},
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
