// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:metal_gate_test/Features/User/presentation/Widgets/signup/form_widget.dart';
import 'package:metal_gate_test/Features/User/presentation/Widgets/splash/button_widget.dart';
import 'package:metal_gate_test/core/colors/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SignUpPage extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController userNameController = TextEditingController();
  SignUpPage({super.key});
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(color: AppColors.blueColor),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  IconButton(
                    constraints: BoxConstraints(
                      minWidth: 25.w,
                      minHeight: 25.w,
                    ),
                    padding: const EdgeInsets.all(0),
                    iconSize: 20.r,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: AppColors.blueColor,
                    ),
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                      AppColors.whiteColor,
                    )),
                  ),
                  Text(
                    "Welcome,",
                    style: TextStyle(
                        fontSize: 20.sp,
                        color: AppColors.whiteColor,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 230.w,
                    child: Text(
                      "We would like to know you a bit more",
                      style: TextStyle(
                        fontSize: 18.sp,
                        color: AppColors.whiteColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 20.w,
                vertical: 30.h,
              ),
              height: 450.h,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: AppColors.whiteColor,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30.r),
                  topLeft: Radius.circular(30.r),
                ),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 100.w,
                        child: Text(
                          "New Account",
                          style: TextStyle(
                              height: 0,
                              fontSize: 20.sp,
                              color: AppColors.blackColor,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      const Spacer(),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.camera,
                          color: AppColors.whiteColor,
                        ),
                        style: OutlinedButton.styleFrom(
                          shape: const CircleBorder(),
                          backgroundColor: AppColors.lightGreyColor,
                          side: BorderSide(
                            width: 1.r,
                            color: AppColors.greyColor,
                          ), // Border properties
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 8), // Padding
                        ),
                      )
                    ],
                  ),
                  FormWidget(
                    formKey: formKey,
                    emailController: emailController,
                    passwordController: passwordController,
                    phoneController: phoneController,
                    userNameController: userNameController,
                    key: key,
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  SplashButton(
                    onpressed: validateFormThenUpdateOrAddPost,
                    text: "SIGN UP",
                    backgroundColor: AppColors.organeColor,
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Already have an account? ",
                        style: TextStyle(
                          height: 0,
                          fontSize: 10.sp,
                          color: AppColors.greyColor,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Log in",
                          style: TextStyle(
                            height: 0,
                            fontSize: 10.sp,
                            color: AppColors.blueColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  validateFormThenUpdateOrAddPost() {
    final isValid = formKey.currentState!.validate();
    if (isValid) {
    } else {}
  }
}
