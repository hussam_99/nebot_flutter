// ignore_for_file: depend_on_referenced_packages, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:metal_gate_test/core/colors/app_colors.dart';

class SocialMediaButtonWidget extends StatelessWidget {
  final IconData socialIcon;
  final Function() onPressed;
  const SocialMediaButtonWidget({
    super.key,
    required this.socialIcon,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton.outlined(
        style: OutlinedButton.styleFrom(
          shape: CircleBorder(),
          backgroundColor: AppColors.transpatenColor,
          side: BorderSide(
              width: 1.r, color: AppColors.whiteColor), // Border properties
          padding: const EdgeInsets.symmetric(
              horizontal: 16, vertical: 8), // Padding
        ),
        onPressed: onPressed,
        icon: Icon(
          socialIcon,
          color: AppColors.whiteColor,
        ));
  }
}
