// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:metal_gate_test/core/colors/app_colors.dart';

class SplashButton extends StatelessWidget {
  final String text;

  final Color? backgroundColor;
  final Color? borderColor;
  final Function() onpressed;
  const SplashButton({
    super.key,
    this.backgroundColor,
    required this.onpressed,
    required this.text,
    this.borderColor,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
        style: OutlinedButton.styleFrom(
          minimumSize: Size(250.w, 45.h),
          backgroundColor: backgroundColor,
          side: BorderSide(
            width: 2,
            color: borderColor ?? AppColors.transpatenColor,
          ), // Border properties
          padding: const EdgeInsets.symmetric(
              horizontal: 16, vertical: 8), // Padding
        ),
        onPressed: onpressed,
        child: Text(
          text,
          style: TextStyle(
            color: AppColors.whiteColor,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ));
  }
}
