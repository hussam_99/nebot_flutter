import 'package:flutter/material.dart';
import 'package:metal_gate_test/Features/User/presentation/Widgets/signup/form_field_widget.dart';

class FormWidget extends StatelessWidget {
  final TextEditingController emailController;
  final TextEditingController phoneController;
  final TextEditingController passwordController;
  final TextEditingController userNameController;
  const FormWidget({
    super.key,
    required this.formKey,
    required this.emailController,
    required this.phoneController,
    required this.passwordController,
    required this.userNameController,
  });

  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          FormTextFieldWidget(
            textFieldController: emailController, //TextEditingController(),
            hintText: "EMAIL",
          ),
          FormTextFieldWidget(
            textFieldController: userNameController, //TextEditingController(),
            hintText: "USERNAME",
          ),
          FormTextFieldWidget(
            textFieldController: passwordController, //TextEditingController(),
            hintText: "PASSWORD",
          ),
          FormTextFieldWidget(
            textFieldController: phoneController, //TextEditingController(),
            hintText: "PHONE NUMBER",
          ),
        ],
      ),
    );
  }
}
