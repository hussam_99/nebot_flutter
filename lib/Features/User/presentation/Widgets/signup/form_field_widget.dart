// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:metal_gate_test/core/colors/app_colors.dart';

class FormTextFieldWidget extends StatelessWidget {
  const FormTextFieldWidget({
    super.key,
    required this.textFieldController,
    required this.hintText,
    this.minLines,
    this.maxLines,
  });
  final TextEditingController textFieldController;
  final String hintText;
  final int? minLines;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 2.w, horizontal: 20.w),
      child: TextFormField(
        maxLines: maxLines,
        minLines: minLines,
        controller: textFieldController,
        validator: (value) => value!.isEmpty ? "field can't be empty" : null,
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: AppColors.greyColor,
          )),
          contentPadding: EdgeInsets.symmetric(
            horizontal: 15.w,
            vertical: 0,
          ),
          hintText: hintText,
          hintStyle: TextStyle(
            color: AppColors.lightGreyColor,
            fontSize: 10.sp,
          ),
        ),
      ),
    );
  }
}
