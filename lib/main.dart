// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:metal_gate_test/core/routes/app_route.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      // 1290 x 2796
      // designSize: const Size(1290, 2796),
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,

      builder: (_, child) {
        return MaterialApp(
          routes: routes,
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          // home: SplashPage(),
        );
      },
    );

    /*  */
  }
}
