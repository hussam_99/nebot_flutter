// ignore_for_file: depend_on_referenced_packages

import 'package:get_it/get_it.dart';
import 'package:metal_gate_test/Features/User/data/DataSources/user_local_data_source.dart';
import 'package:metal_gate_test/Features/User/data/DataSources/user_remote_date_source.dart';
import 'package:metal_gate_test/Features/User/data/Repositories/user_repo_implement.dart';
import 'package:metal_gate_test/Features/User/domain/Repositories/user_repo.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/get_all_users_use_case.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/log_in_use_case.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/log_out_use_case.dart';
import 'package:metal_gate_test/Features/User/domain/UseCases/sign_up_use_case.dart';
import 'package:metal_gate_test/Features/User/presentation/Bloc/user_auth/user_auth_bloc.dart';
import 'core/network/network_info.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

final sl = GetIt.instance;

Future<void> init() async {
//Register Bloc

  sl.registerFactory(() => UserAuthBloc(sl(), sl(), sl(), sl()));

//UseCases //make only one object
  sl.registerLazySingleton(() => GetAllUsersUseCase(sl()));
  sl.registerLazySingleton(() => SignUpUseCase(sl()));
  sl.registerLazySingleton(() => LogInUseCase(sl()));
  sl.registerLazySingleton(() => LogOutUseCase(sl()));
//Repo //Abstract class
  sl.registerLazySingleton<UserRepo>(() => UserRepoImplement(
        networkInfo: sl(),
        userRemoteDataSource: sl(),
        userLocalDataSource: sl(),
      ));
//DataSources
  sl.registerLazySingleton<UserRemoteDataSource>(() =>
      UserRemoteDataSourceImplement(client: sl(), userLocalDataSource: sl()));
  sl.registerLazySingleton<UserLocalDataSource>(
      () => PostLocalDataSourceImplement(sl()));

//Core
  sl.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImplement(sl()),
  );

//External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
