import 'package:flutter/material.dart';
import 'package:metal_gate_test/core/colors/app_colors.dart';

class LaodingWidget extends StatelessWidget {
  const LaodingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 200,
      ),
      child: SizedBox(
        width: 200,
        height: 200,
        child: CircularProgressIndicator(
          color: AppColors.blueColor,
        ),
      ),
    );
  }
}
