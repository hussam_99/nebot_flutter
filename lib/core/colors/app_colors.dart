import 'package:flutter/material.dart';

class AppColors {
  static Color organeColor = const Color(0xfffd9c00);
  static Color blueColor = const Color(0xFF0368ff);
  static Color whiteColor = Colors.white;
  static Color transpatenColor = Colors.transparent;
  static Color greyColor = Colors.grey;
  static Color lightGreyColor = Colors.grey.shade300;
  static Color greenColor = Colors.green;
  static Color redColor = Colors.red;
  static Color blackColor = Colors.black;
}
