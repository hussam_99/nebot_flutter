import 'package:metal_gate_test/Features/User/presentation/Pages/Auth/sign_up_page.dart';
import 'package:metal_gate_test/Features/User/presentation/Pages/splash/splash_page.dart';

var routes = {
  '/': (context) => const SplashPage(),
  '/signUp': (context) => SignUpPage(),
};
