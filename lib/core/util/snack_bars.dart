import 'package:flutter/material.dart';
import 'package:metal_gate_test/core/colors/app_colors.dart';

class SnackBarMessage {
  void showSuccessSnackBar({
    required String message,
    required BuildContext context,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: TextStyle(
            fontSize: 10,
            color: AppColors.whiteColor,
          ),
        ),
        backgroundColor: AppColors.greenColor,
      ),
    );
  }

  void showErrorSnackBar({
    required String message,
    required BuildContext context,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: TextStyle(
            fontSize: 10,
            color: AppColors.whiteColor,
          ),
        ),
        backgroundColor: AppColors.redColor,
      ),
    );
  }
}
